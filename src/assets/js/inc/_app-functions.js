const WOW = require('wowjs');

let appFunctions = (function ($, window, undefined) {
    'use strict';
    let $win = $(window);

    /*-----------------------------------------------------*/
    /*------------------------  init function  --------------------*/
    /*-----------------------------------------------------*/

    function _initFunction() {
        _language();
        _main_menu();
        _navBarState();
        _clickMenu();
        _scroll();
        _wowJs();
        _chartHover();
    }

    /*-----------------------------------------------------*/
    /*------------------------  _language function  --------------------*/
    /*-----------------------------------------------------*/

    function _chartHover() {
        $(document).on('mouseenter touchstart', '.chart-item path', function (e) {
            e.preventDefault();
            let _id = $(this).attr('id');
            $(this).addClass('is-active').siblings('.is-active').removeClass('is-active');
            $('g[data-chart].is-active').removeClass('is-active');
            $('[data-chart="' + _id + '"]').addClass('is-active');
        });

        $(document).on('mouseleave', '.chart-item path', function (e) {
            e.preventDefault();
            $('.chart-item path.is-active').removeClass('is-active');
            $('g[data-chart].is-active').removeClass('is-active');
        });

        $(document).on('mouseenter touchstart', '.about-box', function (e) {
            e.preventDefault();
            $(this).find('img').addClass('bounce');
        });

        $(document).on('mouseleave', '.about-box', function (e) {
            e.preventDefault();
            $(this).find('img').removeClass('bounce');
        });
    }

    /*-----------------------------------------------------*/
    /*------------------------  _language function  --------------------*/
    /*-----------------------------------------------------*/

    function _language() {
        const $menu = $('#lg-toggler');

        $(document).mouseup(e => {
           if (!$menu.is(e.target) // if the target of the click isn't the container...
           && $menu.has(e.target).length === 0) // ... nor a descendant of the container
           {
             $(".under").removeClass('is_active');
          }
         });

        $("#lg-toggler").on('click',()=>{
            $(".under").toggleClass('is_active');
        });


    }

    /*-----------------------------------------------------*/
    /*------------------------  _language function  --------------------*/
    /*-----------------------------------------------------*/

    function _main_menu() {
        $("#main-menu").on('click',function(){
            $(".navbar").toggleClass('active');
        });
    }

    /*-----------------------------------------------------*/
    /*------------------------  _navBarState function  --------------------*/
    /*-----------------------------------------------------*/

    function _navBarState() {
        let lastScrollTop = 0;
        $(window).scroll(function(){

            let $this = $(this),
                st = $this.scrollTop(),
                navbar = $('#header');

            if($(window ).width() < 922 ){
                if ( st > 50 ) {
                    navbar.addClass('scrolled');
                } else {
                    navbar.removeClass('scrolled awake');
                }

                if (navbar.hasClass('scrolled') && st > 51 ) {
                    if (st > lastScrollTop){
                        navbar.removeClass('awake');
                        navbar.addClass('sleep');
                    } else {
                        navbar.addClass('awake');
                        navbar.removeClass('sleep');
                    }
                    lastScrollTop = st;
                }
            }else{
                if ( st > 100 ) {
                    navbar.addClass('scrolled');
                } else {
                    navbar.removeClass('scrolled awake');
                }

                if (navbar.hasClass('scrolled') && st > 101 ) {
                    if (st > lastScrollTop){
                        navbar.removeClass('awake');
                        navbar.addClass('sleep');
                    } else {
                        navbar.addClass('awake');
                        navbar.removeClass('sleep');
                    }
                    lastScrollTop = st;
                }
            }
        });

        $('#header')
            .mouseenter(function() {
                let $this = $(this);
                $this.addClass('awake');
                $this.removeClass('sleep');
            })
            .mouseleave(function() {
                let $this = $(this);
                $this.addClass('sleep');
                $this.removeClass('awake');
            });
    }

    /*-----------------------------------------------------*/
    /*------------------------  _clickMenu function  --------------------*/
    /*-----------------------------------------------------*/

    function _clickMenu() {
        let data_id;
        let scroll_data_id;
        //khi click vào item nào thì addClass 'active' và scroll đến section đó với time =500
        $(".navbar-nav li").click(function(){
            $(this).siblings().removeClass('active');
            $(this).addClass('active');
            data_id = $(this).attr('data-id');
            scroll_data_id = $("#"+data_id+"").offset().top;
            $("html, body").animate({scrollTop:scroll_data_id+1}, 500, 'swing', function() {
            });
        });
    }

    /*-----------------------------------------------------*/
    /*------------------------  _scroll function  --------------------*/
    /*-----------------------------------------------------*/

    function _scroll() {
        let offset;
        let offset_id;
        let scroll; 
        $(window).scroll(function(){
            scroll = $(window).scrollTop();
            $("#main-content section").each(function(){
                offset = $(this).offset().top;
                offset_id = $(this).attr("id");

                if(scroll >= offset) {
                    $(".navbar-nav li").siblings().removeClass('active');
                    $(".navbar-nav li[data-id="+offset_id+"").addClass('active');
                }
            });
        });
    }

    /*-----------------------------------------------------*/
    /*------------------------  _wowJs function  --------------------*/
    /*-----------------------------------------------------*/

    function _wowJs() {
        window.wow = new WOW.WOW({
                boxClass:     'wow',      // animated element css class (default is wow)
                animateClass: 'animated', // animation css class (default is animated)
                offset:       0,          // distance to the element when triggering the animation (default is 0)
                mobile:       true,       // trigger animations on mobile devices (default is true)
                live:         true,       // act on asynchronously loaded content (default is true)
                callback:     function(box) {
                    // the callback is fired every time an animation is started
                    // the argument that is passed in is the DOM node being animated
                },
                scrollContainer: null // optional scroll container selector, otherwise use window
        });
        window.wow.init();
    }

    /*-----------------------------------------------------*/
    /*------------------------  export function  ---------------------*/
    /*-----------------------------------------------------*/

    function _exportFunction() {
        console.log('export function');
    }


    return {
        init: function () {
            _initFunction();
        },
        exportFunction: _exportFunction,
    };

}(jQuery, window));

jQuery(document).ready(function () {
    appFunctions.init();
});