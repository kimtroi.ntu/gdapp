<?php if (!defined('APP_PATH')) die('Bad requested!');

function get_ajax_chart()
{
    $lang = $_POST['lang'];
    get_template_part('template-parts/svgs/chart-' . $lang);
    exit; // exit ajax call(or it will return useless information to the response)
}

// Fire AJAX action for both logged in and non-logged in users
add_action('wp_ajax_get_ajax_chart', 'get_ajax_chart');
add_action('wp_ajax_nopriv_get_ajax_chart', 'get_ajax_chart');

function get_ajax_banner()
{
    get_template_part('template-parts/svgs/banner');
    exit; // exit ajax call(or it will return useless information to the response)
}

// Fire AJAX action for both logged in and non-logged in users
add_action('wp_ajax_get_ajax_banner', 'get_ajax_banner');
add_action('wp_ajax_nopriv_get_ajax_banner', 'get_ajax_banner');

function get_ajax_banner_mb()
{
    get_template_part('template-parts/svgs/banner-mb');
    exit; // exit ajax call(or it will return useless information to the response)
}

// Fire AJAX action for both logged in and non-logged in users
add_action('wp_ajax_get_ajax_banner_mb', 'get_ajax_banner_mb');
add_action('wp_ajax_nopriv_get_ajax_banner_mb', 'get_ajax_banner_mb');