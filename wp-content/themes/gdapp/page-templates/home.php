<?php
/**
 * Template Name: Home Page
 */
get_header();
?>
    <!--Start Pull HTML here-->
    <main id="main-content" class="main">
        <?php get_template_part('template-parts/blocks/home/banner') ?>
        <?php get_template_part('template-parts/blocks/home/about') ?>
        <?php get_template_part('template-parts/blocks/home/our') ?>
        <?php get_template_part('template-parts/blocks/home/gtoken') ?>
        <?php get_template_part('template-parts/blocks/home/road-map') ?>
    </main>
    <!--END  Pull HTML here-->
<?php get_footer();

