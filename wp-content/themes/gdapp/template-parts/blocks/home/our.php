<?php
$gallery = get_field('gallery');
$gallery_mobile = get_field('gallery_copy');
?>
<section id="our">
    <div class="container">
        <div class="our-title">
            <h3><span><?= $gallery['title'] ?></h3>
        </div>

        <!-- >= 768px sẽ hiển thị, ngược lại ẩn-->
        <div class="d-none d-md-block">
            <div class="our-box">
                <div class="row">
                    <div class="col-md-8 wow slideInLeft"  data-wow-duration="0.4s">
                        <a href="<?= $gallery['image_1']['link'] ? $gallery['image_1']['link'] : '#' ?>">
                            <img src="<?= wp_get_attachment_url($gallery['image_1']['image']) ?>" alt="<?= get_the_title($gallery['image_1']['image']) ?>">
                        </a>
                    </div>

                    <div class="col-md-4 wow slideInRight"  data-wow-duration="0.4s">
                        <a href="<?= $gallery['image_2']['link'] ? $gallery['image_2']['link'] : '#' ?>">
                            <img src="<?= wp_get_attachment_url($gallery['image_2']['image']) ?>" alt="<?= get_the_title($gallery['image_2']['image']) ?>">
                        </a>
                    </div>
                </div>
            </div>

            <div class="our-box">
                <div class="row">
                    <div class="col-md-4 wow slideInLeft"  data-wow-duration="0.8s">
                        <a href="<?= $gallery['image_3']['link'] ? $gallery['image_3']['link'] : '#' ?>">
                            <img src="<?= wp_get_attachment_url($gallery['image_3']['image']) ?>" alt="<?= get_the_title($gallery['image_3']['image']) ?>">
                        </a>
                    </div>

                    <div class="col-md-4 wow slideInUp"  data-wow-duration="0.8s">
                        <a href="<?= $gallery['image_4']['link'] ? $gallery['image_4']['link'] : '#' ?>">
                            <img src="<?= wp_get_attachment_url($gallery['image_4']['image']) ?>" alt="<?= get_the_title($gallery['image_4']['image']) ?>">
                        </a>
                    </div>

                    <div class="col-md-4 wow slideInRight"  data-wow-duration="0.8s">
                        <a href="<?= $gallery['image_5']['link'] ? $gallery['image_5']['link'] : '#' ?>">
                            <img src="<?= wp_get_attachment_url($gallery['image_5']['image']) ?>" alt="<?= get_the_title($gallery['image_5']['image']) ?>">
                        </a>
                    </div>
                </div>
            </div>

            <div class="our-box">
                <div class="row">
                    <div class="col-md-4 wow slideInLeft"  data-wow-duration="1.2s">
                        <a href="<?= $gallery['image_6']['link'] ? $gallery['image_6']['link'] : '#' ?>">
                            <img src="<?= wp_get_attachment_url($gallery['image_6']['image']) ?>" alt="<?= get_the_title($gallery['image_6']['image']) ?>">
                        </a>
                    </div>

                    <div class="col-md-8 wow slideInRight"  data-wow-duration="1.2s">
                        <a href="<?= $gallery['image_7']['link'] ? $gallery['image_7']['link'] : '#' ?>">
                            <img src="<?= wp_get_attachment_url($gallery['image_7']['image']) ?>" alt="<?= get_the_title($gallery['image_7']['image']) ?>">
                        </a>
                    </div>
                </div>
            </div>
        </div>

        <!-- >= 768px sẽ ẩn, ngược lại hiển thị-->
        <div class="d-block d-md-none">
            <div class="our-box">
                <div class="row">
                    <div class="col-12 wow slideInLeft"  data-wow-duration="0.4s">
                        <a href="<?= $gallery_mobile['image_1']['link'] ? $gallery_mobile['image_1']['link'] : '#' ?>">
                            <img src="<?= wp_get_attachment_url($gallery_mobile['image_1']['image']) ?>" alt="<?= get_the_title($gallery_mobile['image_1']['image']) ?>">
                        </a>
                    </div>
                </div>
            </div>

            <div class="our-box">
                <div class="row">
                    <div class="col-6 wow slideInLeft pr-1"  data-wow-duration="0.4s">
                        <a href="<?= $gallery_mobile['image_2']['link'] ? $gallery_mobile['image_2']['link'] : '#' ?>">
                            <img src="<?= wp_get_attachment_url($gallery_mobile['image_2']['image']) ?>" alt="<?= get_the_title($gallery_mobile['image_2']['image']) ?>">
                        </a>
                    </div>

                    <div class="col-6 wow slideInRight  pl-1"  data-wow-duration="0.8s">
                        <a href="<?= $gallery_mobile['image_3']['link'] ? $gallery_mobile['image_3']['link'] : '#' ?>">
                            <img src="<?= wp_get_attachment_url($gallery_mobile['image_3']['image']) ?>" alt="<?= get_the_title($gallery_mobile['image_3']['image']) ?>">
                        </a>
                    </div>
                </div>
            </div>

            <div class="our-box">
                <div class="row">
                    <div class="col-md-8 wow slideInRight"  data-wow-duration="1.2s">
                        <a href="<?= $gallery_mobile['image_4']['link'] ? $gallery_mobile['image_4']['link'] : '#' ?>">
                            <img src="<?= wp_get_attachment_url($gallery_mobile['image_4']['image']) ?>" alt="<?= get_the_title($gallery_mobile['image_4']['image']) ?>">
                        </a>
                    </div>
                </div>
            </div>

            <div class="our-box">
                <div class="row">
                    <div class="col-6 wow slideInLeft pr-1"  data-wow-duration="0.8s">
                        <a href="<?= $gallery_mobile['image_5']['link'] ? $gallery_mobile['image_5']['link'] : '#' ?>">
                            <img src="<?= wp_get_attachment_url($gallery_mobile['image_5']['image']) ?>" alt="<?= get_the_title($gallery_mobile['image_5']['image']) ?>">
                        </a>
                    </div>

                    <div class="col-6 wow slideInRight pl-1"  data-wow-duration="0.8s">
                        <a href="<?= $gallery_mobile['image_6']['link'] ? $gallery_mobile['image_6']['link'] : '#' ?>">
                            <img src="<?= wp_get_attachment_url($gallery_mobile['image_6']['image']) ?>" alt="<?= get_the_title($gallery_mobile['image_6']['image']) ?>">
                        </a>
                    </div>
                </div>
            </div>

            <div class="our-box">
                <div class="row">
                    <div class="col-12 wow slideInLeft"  data-wow-duration="1.2s">
                        <a href="<?= $gallery_mobile['image_7']['link'] ? $gallery_mobile['image_7']['link'] : '#' ?>">
                            <img src="<?= wp_get_attachment_url($gallery_mobile['image_7']['image']) ?>" alt="<?= get_the_title($gallery_mobile['image_7']['image']) ?>">
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>