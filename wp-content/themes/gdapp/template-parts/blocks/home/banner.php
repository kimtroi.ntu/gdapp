<section id="banner">
    <div class="bg-banner">
        <img class="bg-img" src="<?= ASSETS_PATH ?>images/banner.png" alt="banner-image">
        <div id="svg-banner" class="d-none d-md-block"></div>
        <div id="svg-banner-mb" class="d-block d-md-none"></div>
        <div class="banner-title">
            <h1><?= get_field('banner_title') ?></h1>
        </div>
        <div class="btn-banner">
            <?php get_template_part('template-parts/components/main-button') ?>
        </div>
        <script type="text/javascript" charset="utf-8" async defer>
            jQuery(document).ready(function($) {
                $.ajax({
                    type: 'POST',
                    url: '<?php echo admin_url('admin-ajax.php');?>',
                    dataType: "html", // add data type
                    data: { action : 'get_ajax_banner' },
                    success: function( response ) {
                        $('#svg-banner').append( response );
                    }
                });
                $.ajax({
                    type: 'POST',
                    url: '<?php echo admin_url('admin-ajax.php');?>',
                    dataType: "html", // add data type
                    data: { action : 'get_ajax_banner_mb' },
                    success: function( response ) {
                        $('#svg-banner-mb').append( response );
                    }
                });
            });
        </script>
    </div>
</section>