<?php $gtoken = get_field('gtoken'); ?>
<section id="gtoken">
    <div class="container">
        <div class="gtoken-title">
            <h3><?= $gtoken['title'] ?></h3>
            <h2><?= $gtoken['subtitle'] ?></h2>
        </div>

        <div class="gtoken-table">
            <?php for ($i=1; $i<=4; $i++) : ?>
                <div class="col-table">
                    <div class="title">
                        <?= $gtoken['table']['col_'.$i]['name'] ?>
                    </div>
                    <div class="descript">
                        <?= $gtoken['table']['col_'.$i]['value'] ?>
                    </div>
                </div>
            <?php endfor; ?>
        </div>

        <div class="gtoken-box">
            <div class="row align-items-center">
                <div class="col-lg-5">
                    <div class="token-info">
                        <h2><?= $gtoken['content_left']['title'] ?></h2>
                        <?= $gtoken['content_left']['content'] ?>
                    </div>

                    <?php get_template_part('template-parts/components/main-button') ?>
                </div>

                <div class="col-lg-7">
                    <div id="enChart"></div>
                    <script type="text/javascript" charset="utf-8" async defer>
                        jQuery(document).ready(function($) {
                            $.ajax({
                                type: 'POST',
                                url: '<?php echo admin_url('admin-ajax.php');?>',
                                dataType: "html", // add data type
                                data: { action : 'get_ajax_chart', lang: '<?= pll_current_language('slug') ?>' },
                                success: function( response ) {
                                    $('#enChart').append( response );
                                }
                            });
                        });
                    </script>
                </div>
            </div>
        </div>
    </div>
</section>