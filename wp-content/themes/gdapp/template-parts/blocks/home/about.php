<?php $vision = get_field('vision'); ?>
<section id="about">
    <div class="container">
        <div class="about-title">
            <h3><?= $vision['title'] ?></h3>
            <h2><?= $vision['subtitle'] ?></h2>
        </div>
        <div class="row">
            <div class="col-md-6">
                <div class="about-box">
                    <img class="wow" data-wow-delay="50ms" data-wow-duration="2s" src="<?= $vision['content_left']['icon'] ?>" alt="<?= $vision['content_left']['title'] ?>">
                    <h4><?= $vision['content_left']['title'] ?></h4>
                    <p><?= $vision['content_left']['description'] ?></p>
                </div>
            </div>
            <div class="col-md-6">
                <div class="about-box">
                    <img class="wow" data-wow-delay="50ms" data-wow-duration="2s" src="<?= $vision['content_right']['icon'] ?>" alt="<?= $vision['content_right']['title'] ?>">
                    <h4><?= $vision['content_right']['title'] ?></h4>
                    <p><?= $vision['content_right']['description'] ?></p>
                </div>
            </div>
        </div>
    </div>
</section>