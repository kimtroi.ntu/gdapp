<?php $road_map = get_field('roadmap'); ?>
<section id="roadmap">
    <div class="container">
        <div class="roadmap-title">
            <h3><?= $road_map['title'] ?></h3>
            <h2><?= $road_map['subtitle'] ?></h2>
        </div>

        <div class="roadmap-group d-md-flex">
            <?php for ($i=1; $i<=5; $i++) : ?>
            <div class="box <?= $i%2 === 0 ? 'bottom' : 'top' ?>">
                <div class="circle-border">
                    <div class="arc">
                        <?= $road_map['box_'.$i]['title'] ?>
                    </div>

                    <div class="caption">
                        <p><?= $road_map['box_'.$i]['description'] ?></p>
                    </div>
                </div>
            </div>
            <?php endfor; ?>
        </div>
    </div>
</section>