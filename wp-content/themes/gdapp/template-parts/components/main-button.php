<?php $button = get_field('button'); ?>
<div class="cover">
    <a class="btn-custom download" href="<?= $button['button_1']['url'] ?>"
       target="<?= $button['button_1']['target'] ?>">
        <span class="pr-1 pr-md-2">
            <img src="<?= ASSETS_PATH ?>images/download.png" alt="">
            <img src="<?= ASSETS_PATH ?>images/download-hover.png" class="img-hover" alt="">
        </span>
        <?= $button['button_1']['title'] ?>
    </a>
    <a class="btn-custom default" href="<?= $button['button_2']['url'] ?>"
       target="<?= $button['button_2']['target'] ?>">
        <?= $button['button_2']['title'] ?>
    </a>
</div>